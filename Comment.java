
public class Comment implements Analysable{
	public String message;
	public int starRating;
	
	public Comment(String message, int starRating) {
		this.message=message;
		this.starRating=starRating;
	}

	@Override
	public void AnalyseComment() {
		if (this.starRating>=3) {
			System.out.println("Positive");
		}else {
			System.out.println("Negative");
		}
		
		
	}
	
	
}
